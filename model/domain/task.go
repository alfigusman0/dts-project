package domain

import "time"

type Task struct {
	Id      int
	Tugas   string
	Petugas string
	Waktu   time.Time
	Status  string
}
