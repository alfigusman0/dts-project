package web

import "time"

type TaskUpdateRequest struct {
	Id      int       `validate:"required"`
	Tugas   string    `validate:"required" json:"tugas"`
	Petugas string    `validate:"required,min=1,max=100" json:"petugas"`
	Waktu   time.Time `validate:"required" json:"waktu"`
	Status  string    `validate:"required" json:"status"`
}
