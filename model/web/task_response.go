package web

import "time"

type TaskResponse struct {
	Id      int       `json:"id"`
	Tugas   string    `json:"tugas"`
	Petugas string    `json:"petugas"`
	Waktu   time.Time `json:"waktu"`
	Status  string    `json:"status"`
}
