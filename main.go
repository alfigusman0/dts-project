package main

import (
	"DTS-Project/app"
	"DTS-Project/controller"
	"DTS-Project/helper"
	"DTS-Project/middleware"
	"DTS-Project/repository"
	"DTS-Project/service"
	"github.com/go-playground/validator/v10"
	_ "github.com/go-sql-driver/mysql"
	"net/http"
)

func main() {

	db := app.NewDB()
	validate := validator.New()
	taskRepository := repository.NewTaskRepository()
	taskService := service.NewTaskService(taskRepository, db, validate)
	taskController := controller.NewTaskController(taskService)
	router := app.NewRouter(taskController)

	server := http.Server{
		Addr:    "127.0.0.1:3000",
		Handler: middleware.NewAuthMiddleware(router),
	}

	err := server.ListenAndServe()
	helper.PanicIfError(err)
}
