package helper

import (
	"DTS-Project/model/domain"
	"DTS-Project/model/web"
)

func ToTaskResponse(task domain.Task) web.TaskResponse {
	return web.TaskResponse{
		Id:      task.Id,
		Tugas:   task.Tugas,
		Petugas: task.Petugas,
		Waktu:   task.Waktu,
		Status:  task.Status,
	}
}

func ToTaskResponses(categories []domain.Task) []web.TaskResponse {
	var taskResponses []web.TaskResponse
	for _, task := range categories {
		taskResponses = append(taskResponses, ToTaskResponse(task))
	}
	return taskResponses
}
