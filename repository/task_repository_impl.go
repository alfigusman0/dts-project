package repository

import (
	"DTS-Project/helper"
	"DTS-Project/model/domain"
	"context"
	"database/sql"
	"errors"
)

type TaskRepositoryImpl struct {
}

func NewTaskRepository() TaskRepository {
	return &TaskRepositoryImpl{}
}

func (repository *TaskRepositoryImpl) Save(ctx context.Context, tx *sql.Tx, task domain.Task) domain.Task {
	SQL := "INSERT INTO tbl_task(tugas, petugas, waktu, status) VALUES (?,?,?,?)"
	result, err := tx.ExecContext(ctx, SQL, task.Tugas, task.Petugas, task.Waktu, task.Status)
	helper.PanicIfError(err)

	id, err := result.LastInsertId()
	helper.PanicIfError(err)

	task.Id = int(id)
	return task
}

func (repository *TaskRepositoryImpl) Update(ctx context.Context, tx *sql.Tx, task domain.Task) domain.Task {
	SQL := "UPDATE tbl_task SET tugas=?, petugas=?, waktu=?, status=? WHERE id = ?"
	_, err := tx.ExecContext(ctx, SQL, task.Tugas, task.Petugas, task.Waktu, task.Status, task.Id)
	helper.PanicIfError(err)

	return task
}

func (repository *TaskRepositoryImpl) Delete(ctx context.Context, tx *sql.Tx, task domain.Task) {
	SQL := "DELETE FROM tbl_task WHERE id = ?"
	_, err := tx.ExecContext(ctx, SQL, task.Id)
	helper.PanicIfError(err)
}

func (repository *TaskRepositoryImpl) FindById(ctx context.Context, tx *sql.Tx, taskId int) (domain.Task, error) {
	SQL := "SELECT id, tugas, petugas, waktu, status FROM tbl_task WHERE id = ?"
	rows, err := tx.QueryContext(ctx, SQL, taskId)
	helper.PanicIfError(err)
	defer rows.Close()

	task := domain.Task{}
	if rows.Next() {
		err := rows.Scan(&task.Id, &task.Tugas, &task.Petugas, &task.Waktu, &task.Status)
		helper.PanicIfError(err)
		return task, nil
	} else {
		return task, errors.New("task is not found")
	}
}

func (repository *TaskRepositoryImpl) FindAll(ctx context.Context, tx *sql.Tx) []domain.Task {
	SQL := "SELECT id, tugas, petugas, waktu, status FROM tbl_task"
	rows, err := tx.QueryContext(ctx, SQL)
	helper.PanicIfError(err)
	defer rows.Close()

	var tasks []domain.Task
	for rows.Next() {
		task := domain.Task{}
		err := rows.Scan(&task.Id, &task.Tugas, &task.Petugas, &task.Waktu, &task.Status)
		helper.PanicIfError(err)
		tasks = append(tasks, task)
	}
	return tasks
}
