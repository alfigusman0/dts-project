package service

import (
	"DTS-Project/exception"
	"DTS-Project/helper"
	"DTS-Project/model/domain"
	"DTS-Project/model/web"
	"DTS-Project/repository"
	"context"
	"database/sql"
	"github.com/go-playground/validator/v10"
)

type TaskServiceImpl struct {
	TaskRepository repository.TaskRepository
	DB             *sql.DB
	Validate       *validator.Validate
}

func NewTaskService(taskRepository repository.TaskRepository, DB *sql.DB, validate *validator.Validate) TaskService {
	return &TaskServiceImpl{
		TaskRepository: taskRepository,
		DB:             DB,
		Validate:       validate,
	}
}

func (service *TaskServiceImpl) Create(ctx context.Context, request web.TaskCreateRequest) web.TaskResponse {
	err := service.Validate.Struct(request)
	helper.PanicIfError(err)

	tx, err := service.DB.Begin()
	helper.PanicIfError(err)
	defer helper.CommitOrRollback(tx)

	task := domain.Task{
		Tugas:   request.Tugas,
		Petugas: request.Petugas,
		Waktu:   request.Waktu,
		Status:  request.Status,
	}

	task = service.TaskRepository.Save(ctx, tx, task)

	return helper.ToTaskResponse(task)
}

func (service *TaskServiceImpl) Update(ctx context.Context, request web.TaskUpdateRequest) web.TaskResponse {
	err := service.Validate.Struct(request)
	helper.PanicIfError(err)

	tx, err := service.DB.Begin()
	helper.PanicIfError(err)
	defer helper.CommitOrRollback(tx)

	task, err := service.TaskRepository.FindById(ctx, tx, request.Id)
	if err != nil {
		panic(exception.NewNotFoundError(err.Error()))
	}

	task.Tugas = request.Tugas
	task.Petugas = request.Petugas
	task.Waktu = request.Waktu
	task.Status = request.Status

	task = service.TaskRepository.Update(ctx, tx, task)

	return helper.ToTaskResponse(task)
}

func (service *TaskServiceImpl) Delete(ctx context.Context, taskId int) {
	tx, err := service.DB.Begin()
	helper.PanicIfError(err)
	defer helper.CommitOrRollback(tx)

	task, err := service.TaskRepository.FindById(ctx, tx, taskId)
	if err != nil {
		panic(exception.NewNotFoundError(err.Error()))
	}

	service.TaskRepository.Delete(ctx, tx, task)
}

func (service *TaskServiceImpl) FindById(ctx context.Context, taskId int) web.TaskResponse {
	tx, err := service.DB.Begin()
	helper.PanicIfError(err)
	defer helper.CommitOrRollback(tx)

	task, err := service.TaskRepository.FindById(ctx, tx, taskId)
	if err != nil {
		panic(exception.NewNotFoundError(err.Error()))
	}

	return helper.ToTaskResponse(task)
}

func (service *TaskServiceImpl) FindAll(ctx context.Context) []web.TaskResponse {
	tx, err := service.DB.Begin()
	helper.PanicIfError(err)
	defer helper.CommitOrRollback(tx)

	tasks := service.TaskRepository.FindAll(ctx, tx)

	return helper.ToTaskResponses(tasks)
}
