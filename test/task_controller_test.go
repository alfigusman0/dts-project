package test

import (
	"DTS-Project/app"
	"DTS-Project/controller"
	"DTS-Project/helper"
	"DTS-Project/middleware"
	"DTS-Project/model/domain"
	"DTS-Project/repository"
	"DTS-Project/service"
	"context"
	"database/sql"
	"encoding/json"
	"fmt"
	"github.com/go-playground/validator/v10"
	_ "github.com/go-sql-driver/mysql"
	"github.com/stretchr/testify/assert"
	"io"
	"net/http"
	"net/http/httptest"
	"strconv"
	"strings"
	"testing"
	"time"
)

func setupTestDB() *sql.DB {
	db, err := sql.Open("mysql", "dts_tugas:1Sampai9!@tcp(localhost:3306)/dts_tugas_test")
	helper.PanicIfError(err)

	db.SetMaxIdleConns(5)
	db.SetMaxOpenConns(20)
	db.SetConnMaxLifetime(60 * time.Minute)
	db.SetConnMaxIdleTime(10 * time.Minute)

	return db
}

func setupRouter(db *sql.DB) http.Handler {
	validate := validator.New()
	taskRepository := repository.NewTaskRepository()
	taskService := service.NewTaskService(taskRepository, db, validate)
	taskController := controller.NewTaskController(taskService)
	router := app.NewRouter(taskController)

	return middleware.NewAuthMiddleware(router)
}

func truncateTask(db *sql.DB) {
	db.Exec("TRUNCATE tbl_task")
}

func TestCreateTaskSuccess(t *testing.T) {
	db := setupTestDB()
	truncateTask(db)
	router := setupRouter(db)

	requestBody := strings.NewReader(`{"name" : "Gadget"}`)
	request := httptest.NewRequest(http.MethodPost, "http://localhost:3000/api/categories", requestBody)
	request.Header.Add("Content-Type", "application/json")
	request.Header.Add("X-API-Key", "RAHASIA")

	recorder := httptest.NewRecorder()

	router.ServeHTTP(recorder, request)

	response := recorder.Result()
	assert.Equal(t, 200, response.StatusCode)

	body, _ := io.ReadAll(response.Body)
	var responseBody map[string]interface{}
	json.Unmarshal(body, &responseBody)

	assert.Equal(t, 200, int(responseBody["code"].(float64)))
	assert.Equal(t, "OK", responseBody["status"])
	assert.Equal(t, "", responseBody["message"])
	assert.Equal(t, "Tugas Test", responseBody["data"].(map[string]interface{})["tugas"])
	assert.Equal(t, "Petugas Test", responseBody["data"].(map[string]interface{})["petugas"])
	assert.Equal(t, time.Now(), responseBody["data"].(map[string]interface{})["waktu"])
	assert.Equal(t, "pendding", responseBody["data"].(map[string]interface{})["status"])
}

func TestCreateTaskFailed(t *testing.T) {
	db := setupTestDB()
	truncateTask(db)
	router := setupRouter(db)

	requestBody := strings.NewReader(`{"name" : ""}`)
	request := httptest.NewRequest(http.MethodPost, "http://localhost:3000/api/categories", requestBody)
	request.Header.Add("Content-Type", "application/json")
	request.Header.Add("X-API-Key", "RAHASIA")

	recorder := httptest.NewRecorder()

	router.ServeHTTP(recorder, request)

	response := recorder.Result()
	assert.Equal(t, 400, response.StatusCode)

	body, _ := io.ReadAll(response.Body)
	var responseBody map[string]interface{}
	json.Unmarshal(body, &responseBody)

	assert.Equal(t, 400, int(responseBody["code"].(float64)))
	assert.Equal(t, "BAD REQUEST", responseBody["status"])
	assert.Equal(t, "bad", responseBody["message"])
}

func TestUpdateTaskSuccess(t *testing.T) {
	db := setupTestDB()
	truncateTask(db)

	tx, _ := db.Begin()
	taskRepository := repository.NewTaskRepository()
	task := taskRepository.Save(context.Background(), tx, domain.Task{
		Tugas:   "Tugas Test",
		Petugas: "Petugas Test",
		Waktu:   time.Now(),
		Status:  "pendding",
	})
	tx.Commit()

	router := setupRouter(db)

	requestBody := strings.NewReader(`{"name" : "Gadget"}`)
	request := httptest.NewRequest(http.MethodPut, "http://localhost:3000/api/categories/"+strconv.Itoa(task.Id), requestBody)
	request.Header.Add("Content-Type", "application/json")
	request.Header.Add("X-API-Key", "RAHASIA")

	recorder := httptest.NewRecorder()

	router.ServeHTTP(recorder, request)

	response := recorder.Result()
	assert.Equal(t, 200, response.StatusCode)

	body, _ := io.ReadAll(response.Body)
	var responseBody map[string]interface{}
	json.Unmarshal(body, &responseBody)

	assert.Equal(t, 200, int(responseBody["code"].(float64)))
	assert.Equal(t, "OK", responseBody["status"])
	assert.Equal(t, "", responseBody["message"])
	assert.Equal(t, task.Id, int(responseBody["data"].(map[string]interface{})["id"].(float64)))
	assert.Equal(t, "Tugas Test", responseBody["data"].(map[string]interface{})["tugas"])
	assert.Equal(t, "Petugas Test", responseBody["data"].(map[string]interface{})["petugas"])
	assert.Equal(t, time.Now(), responseBody["data"].(map[string]interface{})["waktu"])
	assert.Equal(t, "pendding", responseBody["data"].(map[string]interface{})["status"])
}

func TestUpdateTaskFailed(t *testing.T) {
	db := setupTestDB()
	truncateTask(db)

	tx, _ := db.Begin()
	taskRepository := repository.NewTaskRepository()
	task := taskRepository.Save(context.Background(), tx, domain.Task{
		Tugas:   "Tugas Test",
		Petugas: "Petugas Test",
		Waktu:   time.Now(),
		Status:  "pendding",
	})
	tx.Commit()

	router := setupRouter(db)

	requestBody := strings.NewReader(`{"name" : ""}`)
	request := httptest.NewRequest(http.MethodPut, "http://localhost:3000/api/categories/"+strconv.Itoa(task.Id), requestBody)
	request.Header.Add("Content-Type", "application/json")
	request.Header.Add("X-API-Key", "RAHASIA")

	recorder := httptest.NewRecorder()

	router.ServeHTTP(recorder, request)

	response := recorder.Result()
	assert.Equal(t, 400, response.StatusCode)

	body, _ := io.ReadAll(response.Body)
	var responseBody map[string]interface{}
	json.Unmarshal(body, &responseBody)

	assert.Equal(t, 400, int(responseBody["code"].(float64)))
	assert.Equal(t, "BAD REQUEST", responseBody["status"])
	assert.Equal(t, "bad message", responseBody["message"])
}

func TestGetTaskSuccess(t *testing.T) {
	db := setupTestDB()
	truncateTask(db)

	tx, _ := db.Begin()
	taskRepository := repository.NewTaskRepository()
	task := taskRepository.Save(context.Background(), tx, domain.Task{
		Tugas:   "Tugas Test",
		Petugas: "Petugas Test",
		Waktu:   time.Now(),
		Status:  "pendding",
	})
	tx.Commit()

	router := setupRouter(db)

	request := httptest.NewRequest(http.MethodGet, "http://localhost:3000/api/categories/"+strconv.Itoa(task.Id), nil)
	request.Header.Add("X-API-Key", "RAHASIA")

	recorder := httptest.NewRecorder()

	router.ServeHTTP(recorder, request)

	response := recorder.Result()
	assert.Equal(t, 200, response.StatusCode)

	body, _ := io.ReadAll(response.Body)
	var responseBody map[string]interface{}
	json.Unmarshal(body, &responseBody)

	assert.Equal(t, 200, int(responseBody["code"].(float64)))
	assert.Equal(t, "OK", responseBody["status"])
	assert.Equal(t, "", responseBody["message"])
	assert.Equal(t, task.Id, int(responseBody["data"].(map[string]interface{})["id"].(float64)))
	assert.Equal(t, task.Tugas, responseBody["data"].(map[string]interface{})["tugas"])
	assert.Equal(t, task.Petugas, responseBody["data"].(map[string]interface{})["petugas"])
	assert.Equal(t, task.Waktu, responseBody["data"].(map[string]interface{})["waktu"])
	assert.Equal(t, task.Status, responseBody["data"].(map[string]interface{})["status"])
}

func TestGetTaskFailed(t *testing.T) {
	db := setupTestDB()
	truncateTask(db)
	router := setupRouter(db)

	request := httptest.NewRequest(http.MethodGet, "http://localhost:3000/api/categories/404", nil)
	request.Header.Add("X-API-Key", "RAHASIA")

	recorder := httptest.NewRecorder()

	router.ServeHTTP(recorder, request)

	response := recorder.Result()
	assert.Equal(t, 404, response.StatusCode)

	body, _ := io.ReadAll(response.Body)
	var responseBody map[string]interface{}
	json.Unmarshal(body, &responseBody)

	assert.Equal(t, 404, int(responseBody["code"].(float64)))
	assert.Equal(t, "NOT FOUND", responseBody["status"])
}

func TestDeleteTaskSuccess(t *testing.T) {
	db := setupTestDB()
	truncateTask(db)

	tx, _ := db.Begin()
	taskRepository := repository.NewTaskRepository()
	task := taskRepository.Save(context.Background(), tx, domain.Task{
		Tugas:   "Tugas Test",
		Petugas: "Petugas Test",
		Waktu:   time.Now(),
		Status:  "pendding",
	})
	tx.Commit()

	router := setupRouter(db)

	request := httptest.NewRequest(http.MethodDelete, "http://localhost:3000/api/categories/"+strconv.Itoa(task.Id), nil)
	request.Header.Add("Content-Type", "application/json")
	request.Header.Add("X-API-Key", "RAHASIA")

	recorder := httptest.NewRecorder()

	router.ServeHTTP(recorder, request)

	response := recorder.Result()
	assert.Equal(t, 200, response.StatusCode)

	body, _ := io.ReadAll(response.Body)
	var responseBody map[string]interface{}
	json.Unmarshal(body, &responseBody)

	assert.Equal(t, 200, int(responseBody["code"].(float64)))
	assert.Equal(t, "OK", responseBody["status"])
}

func TestDeleteTaskFailed(t *testing.T) {
	db := setupTestDB()
	truncateTask(db)
	router := setupRouter(db)

	request := httptest.NewRequest(http.MethodDelete, "http://localhost:3000/api/categories/404", nil)
	request.Header.Add("Content-Type", "application/json")
	request.Header.Add("X-API-Key", "RAHASIA")

	recorder := httptest.NewRecorder()

	router.ServeHTTP(recorder, request)

	response := recorder.Result()
	assert.Equal(t, 404, response.StatusCode)

	body, _ := io.ReadAll(response.Body)
	var responseBody map[string]interface{}
	json.Unmarshal(body, &responseBody)

	assert.Equal(t, 404, int(responseBody["code"].(float64)))
	assert.Equal(t, "NOT FOUND", responseBody["status"])
}

func TestListCategoriesSuccess(t *testing.T) {
	db := setupTestDB()
	truncateTask(db)

	tx, _ := db.Begin()
	taskRepository := repository.NewTaskRepository()
	task1 := taskRepository.Save(context.Background(), tx, domain.Task{
		Tugas:   "Tugas Test",
		Petugas: "Petugas Test",
		Waktu:   time.Now(),
		Status:  "pendding",
	})
	task2 := taskRepository.Save(context.Background(), tx, domain.Task{
		Tugas:   "Tugas Test",
		Petugas: "Petugas Test",
		Waktu:   time.Now(),
		Status:  "pendding",
	})
	tx.Commit()

	router := setupRouter(db)

	request := httptest.NewRequest(http.MethodGet, "http://localhost:3000/api/categories", nil)
	request.Header.Add("X-API-Key", "RAHASIA")

	recorder := httptest.NewRecorder()

	router.ServeHTTP(recorder, request)

	response := recorder.Result()
	assert.Equal(t, 200, response.StatusCode)

	body, _ := io.ReadAll(response.Body)
	var responseBody map[string]interface{}
	json.Unmarshal(body, &responseBody)

	assert.Equal(t, 200, int(responseBody["code"].(float64)))
	assert.Equal(t, "OK", responseBody["status"])

	fmt.Println(responseBody)

	var categories = responseBody["data"].([]interface{})

	taskResponse1 := categories[0].(map[string]interface{})
	taskResponse2 := categories[1].(map[string]interface{})

	assert.Equal(t, task1.Id, int(taskResponse1["id"].(float64)))
	assert.Equal(t, task1.Tugas, taskResponse1["tugas"])
	assert.Equal(t, task1.Petugas, taskResponse1["petugas"])
	assert.Equal(t, task1.Waktu, taskResponse1["waktu"])
	assert.Equal(t, task1.Status, taskResponse1["status"])

	assert.Equal(t, task2.Id, int(taskResponse2["id"].(float64)))
	assert.Equal(t, task2.Tugas, taskResponse2["tugas"])
	assert.Equal(t, task2.Petugas, taskResponse2["petugas"])
	assert.Equal(t, task2.Waktu, taskResponse2["waktu"])
	assert.Equal(t, task2.Status, taskResponse2["status"])
}

func TestUnauthorized(t *testing.T) {
	db := setupTestDB()
	truncateTask(db)
	router := setupRouter(db)

	request := httptest.NewRequest(http.MethodGet, "http://localhost:3000/api/categories", nil)
	request.Header.Add("X-API-Key", "SALAH")

	recorder := httptest.NewRecorder()

	router.ServeHTTP(recorder, request)

	response := recorder.Result()
	assert.Equal(t, 401, response.StatusCode)

	body, _ := io.ReadAll(response.Body)
	var responseBody map[string]interface{}
	json.Unmarshal(body, &responseBody)

	assert.Equal(t, 401, int(responseBody["code"].(float64)))
	assert.Equal(t, "UNAUTHORIZED", responseBody["status"])
}
